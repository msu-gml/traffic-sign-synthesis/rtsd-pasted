import os
import numpy as np
import torch
from torch.utils.data import DataLoader
from .dataset import Dataset, DatasetNumpy
from .models import InpaintingModel
from .utils import Progbar, create_dir, stitch_images
from .metrics import PSNR, EdgeAccuracy

from skimage.io import imread, imsave
from skimage.transform import resize








ARTIFACTED_CNT=20

def my_func_artifacted(dataset, file_from, file_to, classes):
    images_croppeds = []
    masks = []
    icon_imgs = []
    icon_masks = []
    scene_imgs = []
    bbox_infos = []
    file_groups = []
    cur_cls = []
    futures = []

    batch_pos = 0
    for file_id in range(file_from, file_to):
        group_start_pos = batch_pos
        for idx in range(ARTIFACTED_CNT):
            futures.append(dataset.load_item_artifacted(file_id))
            batch_pos += 1
        file_groups.append((group_start_pos, batch_pos))
    for res in futures:
        images_cropped, mask, scene_img, bbox_info = res
        images_croppeds.append(images_cropped.transpose((2,0,1)))
        masks.append(mask.transpose((2,0,1)))
        scene_imgs.append(scene_img.transpose((2,0,1)))
        bbox_infos.append(bbox_info)
    
    return images_croppeds, masks, bbox_infos, file_groups


def get_item_by_bbox_artifacted(bbox_info, scene_img, inpainted_crop, context_frac=0.5):
    max_side = max(bbox_info['h'], bbox_info['w'])
    context_size = int(max_side * context_frac) + 1
    context_size_y = context_size + (max_side - bbox_info['h'] + 1) // 2
    context_size_x = context_size + (max_side - bbox_info['w'] + 1) // 2

    scene_min_y = max(0, bbox_info['y'] - context_size_y)
    scene_max_y = min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + context_size_y)
    scene_min_x = max(0, bbox_info['x'] - context_size_x)
    scene_max_x = min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + context_size_x)

    paste_from_y = context_size_y - (bbox_info['y'] - scene_min_y)
    paste_to_y = scene_max_y - scene_min_y + paste_from_y
    paste_from_x = context_size_x - (bbox_info['x'] - scene_min_x)
    paste_to_x = scene_max_x - scene_min_x + paste_from_x    
    
    from skimage.filters import gaussian

    inpainted_crop = resize(inpainted_crop, (bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), mode='constant', anti_aliasing=True).astype(np.float32)
    
    scene_inpaint_from_y, scene_inpaint_to_y, scene_inpaint_from_x, scene_inpaint_to_x = max(0, bbox_info['y'] - 3), min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + 3), max(0, bbox_info['x'] - 3), min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + 3)
    scene_inpaint_from_y4, scene_inpaint_to_y4, scene_inpaint_from_x4, scene_inpaint_to_x4 = max(0, bbox_info['y'] - 6), min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + 6), max(0, bbox_info['x'] - 6), min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + 6)
    
    crop_mask_1 = np.zeros((scene_img.shape[0], scene_img.shape[1], 1))
    crop_mask_1[scene_inpaint_from_y:scene_inpaint_to_y, scene_inpaint_from_x:scene_inpaint_to_x] = 1
    crop_mask_1 = gaussian(crop_mask_1, 2)
    crop_mask_1 = crop_mask_1[scene_inpaint_from_y4:scene_inpaint_to_y4, scene_inpaint_from_x4:scene_inpaint_to_x4]
    
    scene_img[scene_inpaint_from_y4:scene_inpaint_to_y4, scene_inpaint_from_x4:scene_inpaint_to_x4] = inpainted_crop[scene_inpaint_from_y4 - scene_min_y + paste_from_y:scene_inpaint_to_y4 - scene_min_y + paste_from_y, scene_inpaint_from_x4 - scene_min_x + paste_from_x:scene_inpaint_to_x4 - scene_min_x + paste_from_x] * crop_mask_1 + scene_img[scene_inpaint_from_y4:scene_inpaint_to_y4, scene_inpaint_from_x4:scene_inpaint_to_x4] * (1 - crop_mask_1)
    
    return scene_img

def process_file_artifacted(file_groups, bbox_infos, outputs, outputs_with_sign, icon_img, icon_mask, icons_dataset, cur_cls, unsigned_outputs, unsigned_bbox_infos, unsigned_file_groups):
    cur_filenames = []
    cur_bboxes_lists = []
    for file_group, unsigned_file_group in zip(file_groups, unsigned_file_groups):
        cur_bboxes_list = []
        cur_filename = ""
        scene_img = []
        sign_datas = []

        for i in range(unsigned_file_group[0], unsigned_file_group[1]):
            bbox_info = unsigned_bbox_infos[i]
            fname = '../BicycleGAN/data_rtsd/detection_data/' + bbox_info["fname"]
            if i == unsigned_file_group[0]:
                scene_img = imread(fname)
                if scene_img.max() > 2.0:
                    scene_img = scene_img / 255.0
                cur_filename = "inp_" + bbox_info["fname"][:-4] + ".png"
            inpainted_crop = np.clip(unsigned_outputs[i].transpose((1, 2, 0)), 0.0, 1.0)
            scene_img = get_item_by_bbox_artifacted(bbox_info, scene_img, inpainted_crop)
            
        for i in range(file_group[0], file_group[1]):
            bbox_info = bbox_infos[i]
            
            y_from = outputs_with_sign.shape[2] // 4
            x_from = outputs_with_sign.shape[2] // 4
            h_from = icon_img.shape[3]
            w_from = icon_img.shape[3]
            inpainted_crop = np.clip(outputs[i].transpose((1, 2, 0)), 0.0, 1.0)
            sign = np.clip(outputs_with_sign[:,:,y_from:y_from + h_from, x_from:x_from + w_from][i].transpose((1, 2, 0)), 0.0, 1.0)
            sign_mask = np.clip(icon_mask[i].transpose((1, 2, 0)), 0.0, 1.0)
            scene_img, new_bbox, data_to_sign = get_item_by_bbox(bbox_info, scene_img, inpainted_crop, sign, sign_mask)
            new_bbox['sign_class'] = '_'.join(icons_dataset.get_int_to_classes(cur_cls[i]).split('.'))
            cur_bboxes_list.append(new_bbox)
            sign_datas.append(data_to_sign)
        for data_to_sign in sign_datas:
            scene_img = add_sign(scene_img, data_to_sign)
        fname_synt = './nn_output_all_artifacted/' + cur_filename
        scene_img = (np.clip(scene_img, 0, 1.0) * 255).astype(np.uint8)
        imsave(fname_synt, scene_img)
        cur_filenames.append(cur_filename)
        cur_bboxes_lists.append(cur_bboxes_list)
    return cur_filenames, cur_bboxes_lists






def my_func(dataset, file_from, file_to, classes):
    images_croppeds = []
    masks = []
    icon_imgs = []
    icon_masks = []
    scene_imgs = []
    bbox_infos = []
    file_groups = []
    cur_cls = []
    futures = []

    batch_pos = 0
    for file_id in range(file_from, file_to):
        group_start_pos = batch_pos
        for idx in dataset.road_images.filenames_to_idxs[file_id][1]:
            futures.append(dataset.load_item(idx, classes[idx]))
            cur_cls.append(classes[idx])
            batch_pos += 1
        file_groups.append((group_start_pos, batch_pos))
    for res in futures:
        _, images_cropped, mask, icon_img, icon_mask, _, _, _, _, scene_img, bbox_info = res
        images_croppeds.append(images_cropped.transpose((2,0,1)))
        masks.append(mask.transpose((2,0,1)))
        icon_imgs.append(icon_img.transpose((2,0,1)))
        icon_masks.append(icon_mask[:,:,None].transpose((2,0,1)))
        scene_imgs.append(scene_img.transpose((2,0,1)))
        bbox_infos.append(bbox_info)
    return images_croppeds, masks, icon_imgs, icon_masks, scene_imgs, bbox_infos, file_groups, cur_cls

def get_item_by_bbox(bbox_info, scene_img, inpainted_crop, sign, sign_mask, context_frac=0.5):
    max_side = max(bbox_info['h'], bbox_info['w'])
    context_size = int(max_side * context_frac) + 1
    context_size_y = context_size + (max_side - bbox_info['h'] + 1) // 2
    context_size_x = context_size + (max_side - bbox_info['w'] + 1) // 2

    scene_min_y = max(0, bbox_info['y'] - context_size_y)
    scene_max_y = min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + context_size_y)
    scene_min_x = max(0, bbox_info['x'] - context_size_x)
    scene_max_x = min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + context_size_x)

    paste_from_y = context_size_y - (bbox_info['y'] - scene_min_y)
    paste_to_y = scene_max_y - scene_min_y + paste_from_y
    paste_from_x = context_size_x - (bbox_info['x'] - scene_min_x)
    paste_to_x = scene_max_x - scene_min_x + paste_from_x

    sign_y_from = inpainted_crop.shape[0] // 4
    sign_h_fake = sign.shape[0]
    sign_x_from = inpainted_crop.shape[1] // 4
    sign_w_fake = sign.shape[1]
    inpainted_signed = np.zeros((inpainted_crop.shape[0], inpainted_crop.shape[1], 4))
    inpainted_signed[:,:,:3] = inpainted_crop
    
    
    from skimage.filters import gaussian
    #sign_mask = gaussian(sign_mask, 2)
    inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, :3] = sign * sign_mask + inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, :3] * (1.0 - sign_mask)
    inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, 3] = sign_mask[:,:,0]

    inpainted_crop = resize(inpainted_crop, (bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), mode='constant', anti_aliasing=True).astype(np.float32)
    inpainted_signed = resize(inpainted_signed, (bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), mode='constant', anti_aliasing=True).astype(np.float32)

    scene_inpaint_from_y, scene_inpaint_to_y, scene_inpaint_from_x, scene_inpaint_to_x = max(0, bbox_info['y'] - 3), min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + 3), max(0, bbox_info['x'] - 3), min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + 3)
    scene_inpaint_from_y4, scene_inpaint_to_y4, scene_inpaint_from_x4, scene_inpaint_to_x4 = max(0, bbox_info['y'] - 6), min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + 6), max(0, bbox_info['x'] - 6), min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + 6)
    
    crop_mask_1 = np.zeros((scene_img.shape[0], scene_img.shape[1], 1))
    crop_mask_1[scene_inpaint_from_y:scene_inpaint_to_y, scene_inpaint_from_x:scene_inpaint_to_x] = 1
    crop_mask_1 = gaussian(crop_mask_1, 2)
    crop_mask_1 = crop_mask_1[scene_inpaint_from_y4:scene_inpaint_to_y4, scene_inpaint_from_x4:scene_inpaint_to_x4]
    
    scene_img[scene_inpaint_from_y4:scene_inpaint_to_y4, scene_inpaint_from_x4:scene_inpaint_to_x4] = inpainted_crop[scene_inpaint_from_y4 - scene_min_y + paste_from_y:scene_inpaint_to_y4 - scene_min_y + paste_from_y, scene_inpaint_from_x4 - scene_min_x + paste_from_x:scene_inpaint_to_x4 - scene_min_x + paste_from_x] * crop_mask_1 + scene_img[scene_inpaint_from_y4:scene_inpaint_to_y4, scene_inpaint_from_x4:scene_inpaint_to_x4] * (1 - crop_mask_1)
        
    '''
    
    crop_mask_1 = np.zeros((scene_img.shape[0], scene_img.shape[1], 1))
    crop_mask_1[max(0, bbox_info['y'] - 2):min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + 2), max(0, bbox_info['x'] - 2):min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + 2)] = 1
    crop_mask_1 = gaussian(crop_mask_1, 2)
    crop_mask_1 = crop_mask_1[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']]
    
    scene_img[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']] = inpainted_crop[bbox_info['y'] - scene_min_y + paste_from_y:bbox_info['y'] - scene_min_y + paste_from_y + bbox_info['h'], bbox_info['x'] - scene_min_x + paste_from_x:bbox_info['x'] - scene_min_x + paste_from_x + bbox_info['w']] * crop_mask_1 + scene_img[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']] * (1 - crop_mask_1)
    
    #scene_img[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']] = inpainted_crop[bbox_info['y'] - scene_min_y + paste_from_y:bbox_info['y'] - scene_min_y + paste_from_y + bbox_info['h'], bbox_info['x'] - scene_min_x + paste_from_x:bbox_info['x'] - scene_min_x + paste_from_x + bbox_info['w']]
    '''
    
    min_y = np.argwhere(inpainted_signed[:,:,3].sum(axis=1))[0][0] - paste_from_y + scene_min_y
    max_y = np.argwhere(inpainted_signed[:,:,3].sum(axis=1))[-1][0] - paste_from_y + scene_min_y
    min_x = np.argwhere(inpainted_signed[:,:,3].sum(axis=0))[0][0] - paste_from_x + scene_min_x
    max_x = np.argwhere(inpainted_signed[:,:,3].sum(axis=0))[-1][0] - paste_from_x + scene_min_x
    from math import sqrt
    dy = int((max_y - min_y) * (1 - 1 / sqrt(2)) // 2)
    dx = int((max_x - min_x) * (1 - 1 / sqrt(2)) // 2)
    new_bbox = {
        "y" : int(min_y + dy),
        "x" : int(min_x + dx),
        "h" : int(max_y - min_y - 2 * dy),
        "w" : int(max_x - min_x - 2 * dx),
        "ignore" : False,
    }
    
    inpainted_signed[:,:,3] = np.maximum(inpainted_signed[:,:,3], gaussian(inpainted_signed[:,:,3], 1, multichannel=False))

    data_to_sign = (scene_min_y, scene_max_y, scene_min_x, scene_max_x, paste_from_y, paste_to_y, paste_from_x, paste_to_x, inpainted_signed)
    return scene_img, new_bbox, data_to_sign

def add_sign(scene_img, data_to_sign):
    scene_min_y, scene_max_y, scene_min_x, scene_max_x, paste_from_y, paste_to_y, paste_from_x, paste_to_x, inpainted_signed = data_to_sign
    scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x] = scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x] * (1 - inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,3:]) + inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,:3] * inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,3:]
    return scene_img


def process_file(file_groups, bbox_infos, outputs, outputs_with_sign, icon_img, icon_mask, icons_dataset, cur_cls):
    cur_filenames = []
    cur_bboxes_lists = []
    for file_group in file_groups:
        cur_bboxes_list = []
        cur_filename = ""
        scene_img = []
        sign_datas = []
        for i in range(file_group[0], file_group[1]):
            bbox_info = bbox_infos[i]
            fname = '../BicycleGAN/data_rtsd/detection_data/' + bbox_info["fname"]
            if i == file_group[0]:
                scene_img = imread(fname)
                if scene_img.max() > 2.0:
                    scene_img = scene_img / 255.0
                cur_filename = "inp_" + bbox_info["fname"][:-4] + ".png"
                
            y_from = outputs_with_sign.shape[2] // 4
            x_from = outputs_with_sign.shape[2] // 4
            h_from = icon_img.shape[3]
            w_from = icon_img.shape[3]
            inpainted_crop = np.clip(outputs[i].transpose((1, 2, 0)), 0.0, 1.0)
            sign = np.clip(outputs_with_sign[:,:,y_from:y_from + h_from, x_from:x_from + w_from][i].transpose((1, 2, 0)), 0.0, 1.0)
            sign_mask = np.clip(icon_mask[i].transpose((1, 2, 0)), 0.0, 1.0)
            scene_img, new_bbox, data_to_sign = get_item_by_bbox(bbox_info, scene_img, inpainted_crop, sign, sign_mask)
            new_bbox['sign_class'] = '_'.join(icons_dataset.get_int_to_classes(cur_cls[i]).split('.'))
            cur_bboxes_list.append(new_bbox)
            sign_datas.append(data_to_sign)
        for data_to_sign in sign_datas:
            scene_img = add_sign(scene_img, data_to_sign)
        fname_synt = './nn_output_all/' + cur_filename
        scene_img = (np.clip(scene_img, 0, 1.0) * 255).astype(np.uint8)
        imsave(fname_synt, scene_img)
        cur_filenames.append(cur_filename)
        cur_bboxes_lists.append(cur_bboxes_list)
    return cur_filenames, cur_bboxes_lists





def my_func_icons(dataset, idx_from, idx_to, indexes, classes, classes_names):
    images_croppeds = []
    masks = []
    icon_imgs = []
    icon_masks = []
    bbox_infos = []
    cur_cls = []
    cur_cls_str = []
    global_idxs = []
    futures = []

    for idx in range(idx_from, idx_to):
        futures.append(dataset.load_item(indexes[idx], classes[idx]))
        cur_cls.append(classes[idx])
        cur_cls_str.append(classes_names[idx])
        global_idxs.append(idx)
    for res in futures:
        _, images_cropped, mask, icon_img, icon_mask, _, _, _, _, scene_img, bbox_info = res
        images_croppeds.append(images_cropped.transpose((2,0,1)))
        masks.append(mask.transpose((2,0,1)))
        icon_imgs.append(icon_img.transpose((2,0,1)))
        icon_masks.append(icon_mask[:,:,None].transpose((2,0,1)))
        bbox_infos.append(bbox_info)
    
    return images_croppeds, masks, icon_imgs, icon_masks, None, bbox_infos, cur_cls, cur_cls_str, global_idxs
    

def process_file_icons(bbox_infos, inpainteds, sign_synt_masks, cur_cls, cur_cls_str, global_idxs, rnd):
    inpainteds = inpainteds.transpose((0, 2, 3, 1))
    for idx in range(len(bbox_infos)):
        bbox_info = bbox_infos[idx]
        cur_img = inpainteds[idx]
        mask = sign_synt_masks[idx][0]
        min_y = np.argwhere(mask.sum(axis=1))[0][0] + cur_img.shape[0] // 4
        max_y = np.argwhere(mask.sum(axis=1))[-1][0] + cur_img.shape[0] // 4
        min_x = np.argwhere(mask.sum(axis=0))[0][0] + cur_img.shape[1] // 4
        max_x = np.argwhere(mask.sum(axis=0))[-1][0] + cur_img.shape[1] // 4
        dy = -rnd.randint(0, max(1, int(0.1 * (max_y - min_y))))
        dx = -rnd.randint(0, max(1, int(0.1 * (max_x - min_x))))
        min_y = max(0, min_y - dy)
        min_x = max(0, min_x - dx)
        max_y = min(max_y + dy, cur_img.shape[0])
        max_x = min(max_x + dx, cur_img.shape[1])
        cur_img = cur_img[min_y:max_y, min_x:max_x]
        side_mean_need = (bbox_info['h'] + bbox_info['w']) // 2
        side_mean_have = (max_y - min_y + max_x - min_x) // 2
        new_h = (max_y - min_y) * side_mean_need // side_mean_have
        new_w = (max_x - min_x) * side_mean_need // side_mean_have
        
        cur_img = resize(cur_img, (new_h, new_w), mode='constant', anti_aliasing=True).astype(np.float32)
        cur_img = np.clip(cur_img * 255, 0, 255).astype(np.uint8)
        out_fname = './nn_icons/' + cur_cls_str[idx] + '/' + str(global_idxs[idx]) + ".jpg"
        #print(out_fname)
        imsave(out_fname, cur_img)
    return None


class EdgeConnect():
    def __init__(self, config):
        self.config = config

        model_name = 'inpaint'
        
        self.debug = False
        self.model_name = model_name
        self.inpaint_model = InpaintingModel(config).to(config.DEVICE)

        self.psnr = PSNR(255.0).to(config.DEVICE)

        # test mode
        if self.config.MODE == 2:
            #self.test_dataset = Dataset(config, training=False)
            self.test_dataset = DatasetNumpy(config, training=False)
            
            self.val_dataset = Dataset(config, training=True)
            self.sample_iterator = self.val_dataset.create_iterator(config.SAMPLE_SIZE, is_shuffle=True)
        else:
            self.train_dataset = Dataset(config, training=True)
            self.val_dataset = Dataset(config, training=True)
            self.sample_iterator = self.val_dataset.create_iterator(config.SAMPLE_SIZE)

        self.samples_path = os.path.join(config.PATH, 'samples_forfig')
        self.results_path = os.path.join(config.PATH, 'results')

        if config.RESULTS is not None:
            self.results_path = os.path.join(config.RESULTS)

        if config.DEBUG is not None and config.DEBUG != 0:
            self.debug = True

        self.log_file = os.path.join(config.PATH, 'log_' + model_name + '.dat')

    def load(self):
        self.inpaint_model.load()

    def save(self):
        self.inpaint_model.save()

    def train(self):
        train_loader = DataLoader(
            dataset=self.train_dataset,
            batch_size=self.config.BATCH_SIZE,
            num_workers=4,
            drop_last=True,
            shuffle=True
        )

        epoch = 0
        keep_training = True
        model = self.config.MODEL
        max_iteration = int(float((self.config.MAX_ITERS)))
        total = len(self.train_dataset)

        if total == 0:
            print('No training data was provided! Check \'TRAIN_FLIST\' value in the configuration file.')
            return

        while(keep_training):
            epoch += 1
            print('\n\nTraining epoch: %d' % epoch)

            progbar = Progbar(total, width=20, stateful_metrics=['epoch', 'iter'])

            for items in train_loader:
                self.inpaint_model.train()

                images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted = self.cuda(*items)

                # train
                outputs, outputs_with_sign, gen_loss, dis_loss, logs = self.inpaint_model.process(images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted)
                outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))

                # metrics
                psnr = self.psnr(self.postprocess(images), self.postprocess(outputs_merged))
                mae = (torch.sum(torch.abs(images - outputs_merged)) / torch.sum(images)).float()
                logs.append(('psnr', psnr.item()))
                logs.append(('mae', mae.item()))
                logs.append(('gen_loss', gen_loss.detach().item()))
                logs.append(('dis_loss', dis_loss.detach().item()))

                for i in range(10):
                    if not isinstance(logs[i][1], float):
                        logs.append((logs[i][0][2:], logs[i][1].detach()))
                    else:
                        logs.append((logs[i][0][2:], logs[i][1]))

                # backward
                self.inpaint_model.backward(gen_loss, dis_loss)
                iteration = self.inpaint_model.iteration


                if iteration >= max_iteration:
                    keep_training = False
                    break

                logs = [
                    ("epoch", epoch),
                    ("iter", iteration),
                ] + logs

                progbar.add(len(images), values=logs if self.config.VERBOSE else [x for x in logs if not x[0].startswith('l_')])

                # log model at checkpoints
                if self.config.LOG_INTERVAL and iteration % self.config.LOG_INTERVAL == 0:
                    self.log(logs)

                # sample model at checkpoints
                if self.config.SAMPLE_INTERVAL and iteration % self.config.SAMPLE_INTERVAL == 0:
                    self.sample()

                # evaluate model at checkpoints
                if self.config.EVAL_INTERVAL and iteration % self.config.EVAL_INTERVAL == 0:
                    print('\nstart eval...\n')
                    self.eval()

                # save model at checkpoints
                if self.config.SAVE_INTERVAL and iteration % self.config.SAVE_INTERVAL == 0:
                    self.save()

        print('\nEnd training....')

    def eval(self):
        val_loader = DataLoader(
            dataset=self.val_dataset,
            batch_size=self.config.BATCH_SIZE,
            drop_last=True,
            shuffle=True
        )

        model = self.config.MODEL
        total = len(self.val_dataset)

        self.inpaint_model.eval()

        progbar = Progbar(total, width=20, stateful_metrics=['it'])
        iteration = 0

        for items in val_loader:
            iteration += 1
            images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted = self.cuda(*items)

            outputs, outputs_with_sign, gen_loss, dis_loss, logs = self.inpaint_model.process(images, images_cropped, masks, icon_img, icon_mask, real_sign_pasted)
            outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))
            # metrics
            psnr = self.psnr(self.postprocess(images), self.postprocess(outputs_merged))
            mae = (torch.sum(torch.abs(images - outputs_merged)) / torch.sum(images)).float()
            logs.append(('psnr', psnr.item()))
            logs.append(('mae', mae.item()))




            logs = [("it", iteration), ] + logs
            progbar.add(len(images), values=logs)

    def test(self):
        #self.sample()
        #return
        self.test_newdataset_faster_artifacted()
        return
        self.inpaint_model.eval()

        model = self.config.MODEL
        create_dir(self.results_path)

        test_loader = DataLoader(
            dataset=self.test_dataset,
            batch_size=1,
        )

        index = 0
        for items in test_loader:
            name = self.test_dataset.load_name(index)
            index += 1
            _, images_cropped, masks, icon_img, icon_mask, _ = self.cuda(*items)


            outputs, outputs_with_sign_synt, outputs_with_sign = self.inpaint_model(images_cropped, masks, icon_img, icon_mask)
            outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))

            mask_sign_square = torch.zeros_like(outputs_with_sign)
            y_from = outputs_with_sign.shape[2] // 4
            x_from = outputs_with_sign.shape[2] // 4
            h_from = icon_img.shape[3]
            w_from = icon_img.shape[3]
            mask_sign_square[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = icon_mask
            from skimage.filters import gaussian
            mask_sign_square = mask_sign_square.cpu().numpy()
            mask_sign_square = np.maximum(mask_sign_square, gaussian(mask_sign_square, sigma=1))
            mask_sign_square = torch.tensor(mask_sign_square).cuda()
            outputs_with_sign_merged = (outputs_with_sign * mask_sign_square) + (outputs_merged * (1 - mask_sign_square))
            
            
            output = self.postprocess(outputs_with_sign_merged)[0]
            path = os.path.join(self.results_path, name)
            print(index, name)

            imsave(output, path)

            if self.debug:
                edges = self.postprocess(1 - edges)[0]
                masked = self.postprocess(images_cropped * (1 - masks) + masks)[0]
                fname, fext = name.split('.')

                imsave(edges, os.path.join(self.results_path, fname + '_edge.' + fext))
                imsave(masked, os.path.join(self.results_path, fname + '_masked.' + fext))

        print('\nEnd test....')
        

    def test_newdataset_faster_icons(self, types='all'):
        self.inpaint_model.eval()

        model = self.config.MODEL
        create_dir(self.results_path)

        fnames_to_idxs = self.test_dataset.road_images.filenames_to_idxs
        cur_fname = 0
        cur_fname_pos = 0
        data_to_signs = []
        cur_bboxes = []

        import random
        rnd = random.Random(52)
        import os
        all_ids = []
        rear_ids = []
        often_ids = []
        cls_to_ex_cnt = {}
        for idx, elem in enumerate(sorted(os.listdir('/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/'))):
            all_ids.append(idx)
            if len(os.listdir('/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-synt/' + elem)) > 0:
                cls_to_ex_cnt[idx] = len(os.listdir('/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-synt/' + elem))
            else:
                cls_to_ex_cnt[idx] = 1000
            if len(os.listdir('/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/' + elem)) > 0:
                often_ids.append(idx)
            else:
                rear_ids.append(idx)
        def get_classes(type_ids, cls_to_ex_cnt):
            classes = []
            classes_names = []
            indexes = []
            for idx in type_ids:
                cls_name = self.test_dataset.icons_dataset.get_int_to_classes(idx)
                classes.extend([idx for _ in range(cls_to_ex_cnt[idx])])
                indexes.extend([rnd.randint(0, 3000000) for _ in range(cls_to_ex_cnt[idx])])
                classes_names.extend([cls_name for _ in range(cls_to_ex_cnt[idx])])
                if not os.path.isdir('./nn_icons/' + cls_name):
                    os.mkdir('./nn_icons/' + cls_name)
            return classes, indexes, classes_names
        if types == 'all':
            classes, indexes, classes_names = get_classes(all_ids, cls_to_ex_cnt)
        elif types == 'rear':
            classes, indexes, classes_names = get_classes(rear_ids, cls_to_ex_cnt)
        elif types == 'often':
            classes, indexes, classes_names = get_classes(often_ids, cls_to_ex_cnt)
        print(len(all_ids), len(rear_ids), len(often_ids))
        all_examples_cnt = len(classes)
        
        batch_size=64 #5
        new_json = {}
        futures_writing = []
        #all_examples_cnt = 30

        threads_cnt = 8
        import concurrent.futures
        with concurrent.futures.ProcessPoolExecutor(max_workers=2 * threads_cnt) as executor:
            for start_idx in range(0, all_examples_cnt, threads_cnt * batch_size):
                futures = []
                for idx in range(start_idx, min(start_idx + threads_cnt * batch_size, all_examples_cnt), batch_size):
                    futures.append(executor.submit(my_func_icons, self.test_dataset, idx, min(idx + batch_size, all_examples_cnt), indexes, classes, classes_names))

                for future in futures_writing:
                    future.result()
                futures_writing = []

                print(start_idx)
                for future in futures:
                    import gc
                    gc.collect()
                    images_croppeds, masks, icon_imgs, icon_masks, _, bbox_infos, cur_cls, cur_cls_str, global_idxs = future.result()
                    
                    images_croppeds = torch.tensor(images_croppeds).cuda()
                    masks = torch.tensor(masks).cuda()
                    icon_imgs = torch.tensor(icon_imgs).cuda()
                    icon_masks = torch.tensor(icon_masks).cuda()
                    
                    
                    outputs, outputs_with_sign_synt, outputs_with_sign = self.inpaint_model(images_croppeds, masks, icon_imgs, icon_masks)
                    outputs = (outputs * masks) + (images_croppeds * (1 - masks))
                    mask_sign_square = torch.zeros_like(outputs_with_sign)
                    y_from = outputs_with_sign.shape[2] // 4
                    x_from = outputs_with_sign.shape[2] // 4
                    h_from = icon_imgs.shape[3]
                    w_from = icon_imgs.shape[3]
                    mask_sign_square[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = icon_masks
                    outputs = (outputs_with_sign * mask_sign_square) + (outputs * (1 - mask_sign_square))
                    
                    
                    
                    outputs = np.array(outputs.data.cpu())
                    outputs_with_sign_synt = np.array(outputs_with_sign_synt.data.cpu())
                    outputs_with_sign = np.array(outputs_with_sign.data.cpu())
                    icon_imgs = np.array(icon_imgs.data.cpu())
                    icon_masks = np.array(icon_masks.data.cpu())

                    images_croppeds = images_croppeds.data.cpu()
                    masks = masks.data.cpu()
                    del images_croppeds, masks
                    futures_writing.append(executor.submit(process_file_icons, bbox_infos, outputs_with_sign, icon_masks, cur_cls, cur_cls_str, global_idxs, random.Random(global_idxs[0])))

            for future in futures_writing:
                future.result()
            futures_writing = []
        print('\nEnd test create dataset....')        
    
    
    
    
    
    
    
    def test_newdataset_faster_artifacted(self, types='all'):
        self.inpaint_model.eval()

        model = self.config.MODEL
        create_dir(self.results_path)

        fnames_to_idxs = self.test_dataset.road_images.filenames_to_idxs
        cur_fname = 0
        cur_fname_pos = 0
        data_to_signs = []
        cur_bboxes = []
        ans_json = {}
        
        
        all_examples_cnt = len(self.test_dataset)
        all_files_cnt = len(self.test_dataset.road_images.filenames_to_idxs)
        #all_files_cnt = 30
        import random
        rnd = random.Random(52)
        import os
        all_ids = []
        rear_ids = []
        often_ids = []
        for idx, elem in enumerate(sorted(os.listdir('/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/'))):
            all_ids.append(idx)
            if len(os.listdir('/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/' + elem)) > 0:
                often_ids.append(idx)
            else:
                rear_ids.append(idx)
        if types == 'all':
            classes = [all_ids[rnd.randint(0, len(all_ids) - 1)] for _ in range(all_examples_cnt)]
        elif types == 'rear':
            classes = [rear_ids[rnd.randint(0, len(rear_ids) - 1)] for _ in range(all_examples_cnt)]
        elif types == 'often':
            classes = [often_ids[rnd.randint(0, len(often_ids) - 1)] for _ in range(all_examples_cnt)]
        print(len(all_ids), len(rear_ids), len(often_ids))
        
        batch_size=8 #5
        '''
        with open('new_json.json', 'r') as fp:
            import json
            ans_json = json.load(fp)
        already_existing_files = set(os.listdir('./nn_output/'))
        '''
        futures_writing = []
        #all_files_cnt = 30

        threads_cnt = 16
        import concurrent.futures
        with concurrent.futures.ThreadPoolExecutor(max_workers=2 * threads_cnt) as executor:
            for start_file_id in range(0, all_files_cnt, threads_cnt * batch_size): # 19744
                futures = []
                futures_unsigneds = []
                for file_id in range(start_file_id, min(start_file_id + threads_cnt * batch_size, all_files_cnt), batch_size):
                    futures.append(executor.submit(my_func, self.test_dataset, file_id, min(file_id + batch_size, all_files_cnt), classes))
                for file_id in range(start_file_id, min(start_file_id + threads_cnt * batch_size, all_files_cnt), batch_size):
                    futures_unsigneds.append(executor.submit(my_func_artifacted, self.test_dataset, file_id, min(file_id + batch_size, all_files_cnt), classes))

                for future in futures_writing:
                    cur_filenames, cur_bboxes_lists = future.result()
                    for cur_filename, cur_bboxes_list in zip(cur_filenames, cur_bboxes_lists):
                        ans_json[cur_filename] = cur_bboxes_list
                futures_writing = []
                with open("new_json_all_artifacted.json", "w") as fp:
                    import json
                    json.dump(ans_json, fp)      

                print(start_file_id)
                for future_signed, future_unsigned in zip(futures, futures_unsigneds):
                    import gc
                    gc.collect()
                    print("strt")
                    images_croppeds, masks, icon_imgs, icon_masks, _, bbox_infos, file_groups, cur_cls = future_signed.result()
                    
                    images_croppeds = torch.tensor(images_croppeds).cuda()
                    masks = torch.tensor(masks).cuda()
                    icon_imgs = torch.tensor(icon_imgs).cuda()
                    icon_masks = torch.tensor(icon_masks).cuda()
                    
                    outputs, outputs_with_sign_synt, outputs_with_sign = self.inpaint_model(images_croppeds, masks, icon_imgs, icon_masks)
                    outputs = (outputs * masks) + (images_croppeds * (1 - masks))
                    
                    outputs = np.array(outputs.data.cpu())
                    outputs_with_sign_synt = np.array(outputs_with_sign_synt.data.cpu())
                    outputs_with_sign = np.array(outputs_with_sign.data.cpu())
                    icon_imgs = np.array(icon_imgs.data.cpu())
                    icon_masks = np.array(icon_masks.data.cpu())

                    images_croppeds = images_croppeds.data.cpu()
                    masks = masks.data.cpu()
                    del images_croppeds, masks

                    import gc
                    gc.collect()
                    images_croppeds, masks, unsigned_bbox_infos, unsigned_file_groups = future_unsigned.result()
                    
                    images_croppeds = torch.tensor(images_croppeds).cuda()
                    masks = torch.tensor(masks).cuda()
                    
                    unsigned_outputs = self.inpaint_model.forward_inpaint(images_croppeds, masks)
                    unsigned_outputs = (unsigned_outputs * masks) + (images_croppeds * (1 - masks))
                    
                    unsigned_outputs = np.array(unsigned_outputs.data.cpu())

                    images_croppeds = images_croppeds.data.cpu()
                    masks = masks.data.cpu()
                    del images_croppeds, masks
                    
                    futures_writing.append(executor.submit(process_file_artifacted, file_groups, bbox_infos, outputs, outputs_with_sign, icon_imgs, icon_masks, self.test_dataset.icons_dataset, cur_cls, unsigned_outputs, unsigned_bbox_infos, unsigned_file_groups))

            for future in futures_writing:
                cur_filenames, cur_bboxes_lists = future.result()
                for cur_filename, cur_bboxes_list in zip(cur_filenames, cur_bboxes_lists):
                    ans_json[cur_filename] = cur_bboxes_list
            futures_writing = []

        with open("new_json_all_artifacted.json", "w") as fp:
            import json
            json.dump(ans_json, fp)                
                
        print('\nEnd test create dataset....')    
    
    
    
    
    
    def test_newdataset_faster(self, types='all'):
        self.inpaint_model.eval()

        model = self.config.MODEL
        create_dir(self.results_path)

        fnames_to_idxs = self.test_dataset.road_images.filenames_to_idxs
        cur_fname = 0
        cur_fname_pos = 0
        data_to_signs = []
        cur_bboxes = []
        ans_json = {}
        
        
        all_examples_cnt = len(self.test_dataset)
        all_files_cnt = len(self.test_dataset.road_images.filenames_to_idxs)
        #all_files_cnt = 30
        import random
        rnd = random.Random(52)
        import os
        all_ids = []
        rear_ids = []
        often_ids = []
        for idx, elem in enumerate(sorted(os.listdir('/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/'))):
            all_ids.append(idx)
            if len(os.listdir('/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/' + elem)) > 0:
                often_ids.append(idx)
            else:
                rear_ids.append(idx)
        if types == 'all':
            classes = [all_ids[rnd.randint(0, len(all_ids) - 1)] for _ in range(all_examples_cnt)]
        elif types == 'rear':
            classes = [rear_ids[rnd.randint(0, len(rear_ids) - 1)] for _ in range(all_examples_cnt)]
        elif types == 'often':
            classes = [often_ids[rnd.randint(0, len(often_ids) - 1)] for _ in range(all_examples_cnt)]
        print(len(all_ids), len(rear_ids), len(often_ids))
        
        batch_size=32 #5
        futures_writing = []
        #all_files_cnt = 30
        '''
        with open('new_json.json', 'r') as fp:
            import json
            ans_json = json.load(fp)
        already_existing_files = set(os.listdir('./nn_output/'))
        '''
        
        
        threads_cnt = 16
        import concurrent.futures
        with concurrent.futures.ThreadPoolExecutor(max_workers=2 * threads_cnt) as executor:
            for start_file_id in range(0, all_files_cnt, threads_cnt * batch_size): # 19744
                futures = []
                for file_id in range(start_file_id, min(start_file_id + threads_cnt * batch_size, all_files_cnt), batch_size):
                    #fnn = 'autosave01_02_2012_13_45_31.png'
                    #fnn0 = self.test_dataset.road_images.filenames_to_idxs[file_id][0]
                    #fnn1 = self.test_dataset.road_images.filenames_to_idxs[min(file_id + batch_size, all_files_cnt - 1)][0]
                    #if not((fnn0 <= fnn) and (fnn < fnn1)):
                    #    continue
                    #print("haai ", file_id)
                    '''
                    is_need = False
                    for i in range(file_id, min(file_id + batch_size, all_files_cnt)):
                        fname_helper = self.test_dataset.road_images.filenames_to_idxs[i][0]
                        fname_helper = "inp_" + fname_helper[:-4] + ".png"
                        if (fname_helper not in ans_json) or (fname_helper not in already_existing_files):
                            is_need = True
                    if not is_need:
                        continue
                    '''
                    futures.append(executor.submit(my_func, self.test_dataset, file_id, min(file_id + batch_size, all_files_cnt), classes))

                for future in futures_writing:
                    cur_filenames, cur_bboxes_lists = future.result()
                    for cur_filename, cur_bboxes_list in zip(cur_filenames, cur_bboxes_lists):
                        ans_json[cur_filename] = cur_bboxes_list
                futures_writing = []
                with open("new_json_all.json", "w") as fp:
                    import json
                    json.dump(ans_json, fp)      

                print(start_file_id)
                for future in futures:
                    import gc
                    gc.collect()
                    print("strt")
                    images_croppeds, masks, icon_imgs, icon_masks, _, bbox_infos, file_groups, cur_cls = future.result()
                    #print(len(masks))
                    #for i in range(len(masks)):
                    #    print(images_croppeds[i].shape, masks[i].shape, icon_imgs[i].shape, icon_masks[i].shape, )
                    images_croppeds = torch.tensor(images_croppeds).cuda()
                    masks = torch.tensor(masks).cuda()
                    icon_imgs = torch.tensor(icon_imgs).cuda()
                    icon_masks = torch.tensor(icon_masks).cuda()
                    
                    outputs, outputs_with_sign_synt, outputs_with_sign = self.inpaint_model(images_croppeds, masks, icon_imgs, icon_masks)
                    outputs = (outputs * masks) + (images_croppeds * (1 - masks))
                    
                    outputs = np.array(outputs.data.cpu())
                    outputs_with_sign_synt = np.array(outputs_with_sign_synt.data.cpu())
                    outputs_with_sign = np.array(outputs_with_sign.data.cpu())
                    icon_imgs = np.array(icon_imgs.data.cpu())
                    icon_masks = np.array(icon_masks.data.cpu())

                    images_croppeds = images_croppeds.data.cpu()
                    masks = masks.data.cpu()
                    del images_croppeds, masks

                    futures_writing.append(executor.submit(process_file, file_groups, bbox_infos, outputs, outputs_with_sign, icon_imgs, icon_masks, self.test_dataset.icons_dataset, cur_cls))

            for future in futures_writing:
                cur_filenames, cur_bboxes_lists = future.result()
                for cur_filename, cur_bboxes_list in zip(cur_filenames, cur_bboxes_lists):
                    ans_json[cur_filename] = cur_bboxes_list
            futures_writing = []

        with open("new_json_all.json", "w") as fp:
            import json
            json.dump(ans_json, fp)                
                
        print('\nEnd test create dataset....')    
    
    
        
        
    def test_newdataset(self):
        self.inpaint_model.eval()

        model = self.config.MODEL
        create_dir(self.results_path)

        test_loader = DataLoader(
            dataset=self.test_dataset,
            batch_size=1,
        )

        fnames_to_idxs = self.test_dataset.road_images.filenames_to_idxs
        cur_fname = 0
        cur_fname_pos = 0
        data_to_signs = []
        cur_bboxes = []
        ans_json = {}
        
        index = 0
        for items in test_loader:
            if cur_fname == 15:
                break
            name = self.test_dataset.load_name(index)
            index += 1
            cur_fname_pos += 1
            _, images_cropped, masks, icon_img, icon_mask, _, scene_img = self.cuda(*items[:-1])
            bbox_info = items[-1]
            
            def upd_tensor(tens):
                return np.array(tens.data.cpu())[0].transpose((1,2,0))
            if cur_fname_pos == 1:
                outputs_with_sign_merged = upd_tensor(scene_img)

            outputs, outputs_with_sign_synt, outputs_with_sign = self.inpaint_model(images_cropped, masks, icon_img, icon_mask)
            outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))

            y_from = outputs_with_sign.shape[2] // 4
            x_from = outputs_with_sign.shape[2] // 4
            h_from = icon_img.shape[3]
            w_from = icon_img.shape[3]
            #print(outputs_with_sign_merged.shape, upd_tensor(outputs_merged).shape, upd_tensor(outputs_with_sign[:,:,y_from:y_from + h_from, x_from:x_from + w_from]).shape, upd_tensor(icon_mask).shape)
            outputs_with_sign_merged, new_bbox, data_to_sign = get_item_by_bbox(bbox_info, outputs_with_sign_merged, upd_tensor(outputs_merged), upd_tensor(outputs_with_sign[:,:,y_from:y_from + h_from, x_from:x_from + w_from]), upd_tensor(icon_mask))
            new_bbox['sign_class'] = bbox_info['sign_class']
            cur_bboxes.append(new_bbox)
            data_to_signs.append(data_to_sign)
            
            if cur_fname_pos == len(fnames_to_idxs[cur_fname][1]):
                for data_to_sign in data_to_signs:
                    outputs_with_sign_merged = add_sign(outputs_with_sign_merged, data_to_sign)
                name = "inp_" + fnames_to_idxs[cur_fname][0][:-4] + ".png"

                #output = self.postprocess(outputs_with_sign_merged)[0]
                path = os.path.join(self.results_path, name)
                print(index, name)
                outputs_with_sign_merged = (np.clip(outputs_with_sign_merged, 0, 1.0) * 255).astype(np.uint8)
                imsave(path, outputs_with_sign_merged)
                ans_json[name] = cur_bboxes
                cur_fname_pos = 0
                data_to_signs = []
                cur_bboxes = []
                cur_fname += 1

        with open("new_json.json", "w") as fp:
            import json
            json.dump(ans_json, fp)                
                
        print('\nEnd test create dataset....')

    def sample(self, it=None):
        # do not sample when validation set is empty
        if len(self.val_dataset) == 0:
            return

        self.inpaint_model.eval()

        model = self.config.MODEL
        items = next(self.sample_iterator)
        images, images_cropped, masks, icon_img, icon_mask, real_sign_img = self.cuda(*items)

        iteration = self.inpaint_model.iteration
        outputs, outputs_with_sign_synt, outputs_with_sign = self.inpaint_model(images_cropped, masks, icon_img, icon_mask)
        outputs_merged = (outputs * masks) + (images_cropped * (1 - masks))
        
        mask_sign_square = torch.zeros_like(outputs_with_sign)
        y_from = outputs_with_sign.shape[2] // 4
        x_from = outputs_with_sign.shape[2] // 4
        h_from = icon_img.shape[3]
        w_from = icon_img.shape[3]
        mask_sign_square[:,:,y_from:y_from + h_from, x_from:x_from + w_from] = icon_mask
        mask_sign_square_save = mask_sign_square.clone()
        from skimage.filters import gaussian
        mask_sign_square = mask_sign_square.cpu().numpy()
        mask_sign_square = np.maximum(mask_sign_square, gaussian(mask_sign_square, sigma=1))
        mask_sign_square = torch.tensor(mask_sign_square).cuda()
        outputs_with_sign_merged = (outputs_with_sign * mask_sign_square) + (outputs_merged * (1 - mask_sign_square))


        if it is not None:
            iteration = it

        image_per_row = 2
        if self.config.SAMPLE_SIZE <= 6:
            image_per_row = 1

        images = stitch_images(
            self.postprocess(images),
            self.postprocess(masks),
            self.postprocess(images_cropped),
            self.postprocess(outputs),
            self.postprocess(outputs_merged),
            self.postprocess(outputs_with_sign),
            self.postprocess(outputs_with_sign_synt),
            self.postprocess(outputs_with_sign_merged),
            self.postprocess(mask_sign_square_save),
            self.postprocess(icon_img),
            self.postprocess(icon_mask),
            self.postprocess(real_sign_img),
            img_per_row = image_per_row
        )


        path = os.path.join(self.samples_path, self.model_name)
        name = os.path.join(path, str(iteration).zfill(5) + ".png")
        create_dir(path)
        print('\nsaving sample ' + name)
        images.save(name)

    def log(self, logs):
        with open(self.log_file, 'a') as f:
            f.write('%s\n' % ' '.join([str(item[1]) for item in logs]))

    def cuda(self, *args):
        return (item.to(self.config.DEVICE) for item in args)

    def postprocess(self, img):
        # [0, 1] => [0, 255]
        img = img * 255.0
        img = img.permute(0, 2, 3, 1)
        return img.int()
